package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

var appVersion, commit, buildTime string
var versions Versions
var censors = make(map[string]bool)
var mu sync.Mutex
var statusError int

func main() {
	versions.Version = appVersion
	versions.Commit = commit
	versions.BuildTime = buildTime

	fmt.Println(versions.BuildTime, versions.Commit, versions.Version)
	// Create context that listens for the interrupt signal from the OS.
	//SIGINT = Terminal interrupt signal == 2
	//SIGINT = Termination signal  == 15

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	srv := &http.Server{
		Addr: ":8080",
	}

	http.HandleFunc("/api/v1/version", version)
	http.HandleFunc("/api/v1/authors", getAuthors)
	http.HandleFunc("/api/v1/books", getBooks)
	http.HandleFunc("/api/v1/censors", enpSetCensors)

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Listen for the interrupt signal.
	<-ctx.Done()

	// Restore default behavior on the interrupt signal and notify user of shutdown.
	stop()
	log.Println("shutting down gracefully, press Ctrl+C again to force")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 80*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")

	//log.Fatal(http.ListenAndServe(":8080", nil))

}

func version(w http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. only GET method is supported"))
		statusError = http.StatusBadRequest
		GetIpAddress(w, req, &statusError)
		return
	}
	version, err := json.Marshal(Versions{Version: versions.Version, Commit: versions.Commit, BuildTime: versions.BuildTime})
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}
	statusError = http.StatusOK
	GetIpAddress(w, req, &statusError)
	w.Write(version)

	time.Sleep(60 * time.Second)
}

func getAuthors(w http.ResponseWriter, req *http.Request) {
	bookAuthors := Book{}
	name := Name{}

	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	book := req.URL.Query().Get("book")
	if book == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}

	// TODO: ISBN regex check

	url := "https://openlibrary.org/isbn/" + book + ".json"

	body, err := apiCall(url)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(body, &bookAuthors)
	if err != nil {
		fmt.Println("error:", err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}

	respA := make([]RespAuthor, len(bookAuthors.Authors))

	for index := range bookAuthors.Authors {
		url = "https://openlibrary.org" + bookAuthors.Authors[index].Key + ".json"
		body, err = apiCall(url)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err)
			return
		}
		err := json.Unmarshal(body, &name)
		if err != nil {
			fmt.Println("error:", err)
			sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
		}

		bookAuthors.Authors[index].Key = strings.ReplaceAll(bookAuthors.Authors[index].Key, "/authors/", "")

		respA[index].Name = name.Name
		respA[index].Key = bookAuthors.Authors[index].Key
	}

	resp, err := json.Marshal(respA)
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}
	w.Write(resp)
}

func getBooks(w http.ResponseWriter, req *http.Request) {
	//bookAuthors := Book{}
	//name := Name{}
	authorBooks := Entries{}

	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	author := req.URL.Query().Get("author")
	if author == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}
	if isCensored(author) {
		sendError(w, http.StatusBadRequest, errors.New("this author is censored and his/her books will not be shown"))
		return
	}

	url := "https://openlibrary.org/authors/" + author + "/works.json?limit=2"

	body, err := apiCall(url)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		fmt.Println("error:", err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	resp, err := json.Marshal(respB)
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}
	w.Write(resp)
}

func enpSetCensors(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. POST method is supported only"))
		return
	}

	reqCensors := []string{}
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&reqCensors)
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
		return
	}
	setCensors(reqCensors)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Authors added to censor list"))
	fmt.Println(censors)

}

func setCensors(cens []string) {
	mu.Lock()
	for _, c := range cens {
		censors[c] = true
	}
	mu.Unlock()
}

func isCensored(author string) bool {
	mu.Lock()
	defer mu.Unlock()
	return censors[author]
}

func apiCall(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	// !!! nacitava cely []byte do pamate, lepsie bufio.NewReader()
	body, err := ioutil.ReadAll(resp.Body)

	return body, err
}

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

// merge request try
//curl localhost:8080/api/v1/books?author=OL1394244A
/*
func accessLog() {
	GetIpAddress()


}*/

func GetIpAddress(w http.ResponseWriter, r *http.Request, statusError *int) string {
	ip := r.RemoteAddr
	rUrl := r.URL
	met := r.Method
	cd := *statusError
	now := time.Now()
	fmt.Println(ip, rUrl, cd, met, now)

	return ip
}

//func middlewareEndpoint(w http.ResponseWriter, r *http.Request) {}

//	403 forbidden if auth not working
//
// vytvorime funkciu ktora bude zabalovat funkciu
//func encapsulationHandler(endpoint func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {

//}

//func basicAuth(w http.ResponseWriter, req)

/// kill Term pid prikaz na ukoncenie procesu

/*
exit := make(chan os.Signal, 1)
signal.Notify(exit, os.Interrupt, syscall.SIGTERM)
*/
